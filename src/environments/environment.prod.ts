export const environment = {
  production: true,
  api: 'https://prospectos-api.herokuapp.com/api',
  firebaseConfig: {
    apiKey: "AIzaSyC6e7cPwc2FiYIohqqbjyCdTAW4X1ulrgo",
    authDomain: "sistema-prospectos.firebaseapp.com",
    projectId: "sistema-prospectos",
    storageBucket: "sistema-prospectos.appspot.com",
    messagingSenderId: "1008608094120",
    appId: "1:1008608094120:web:5072e29e495706f1041f9d"
  }
};
