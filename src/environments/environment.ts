// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api: 'https://prospectos-api.herokuapp.com/api',
  firebaseConfig: {
    apiKey: "AIzaSyC6e7cPwc2FiYIohqqbjyCdTAW4X1ulrgo",
    authDomain: "sistema-prospectos.firebaseapp.com",
    projectId: "sistema-prospectos",
    storageBucket: "sistema-prospectos.appspot.com",
    messagingSenderId: "1008608094120",
    appId: "1:1008608094120:web:5072e29e495706f1041f9d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
