import { Injectable, EventEmitter } from '@angular/core';
import { Prospecto } from '../models/prospecto.model';

@Injectable({
  providedIn: 'root'
})
export class DatosService {
  chargeProspecto$ = new EventEmitter<Prospecto>();
  loadTable$ = new EventEmitter();
  waiting$ = new EventEmitter();
  constructor() { }
}
