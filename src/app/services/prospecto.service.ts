import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Prospecto } from '../models/prospecto.model';
import { lastValueFrom } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProspectoService {

  private API_URI: string = environment.api;
  constructor(private _http: HttpClient) {
  }
  async getProspectos(){
    let res: any = await lastValueFrom(this._http.get(`${this.API_URI}/prospectos`));
    if (this.isValidResponse(res.status)) {
      return <Prospecto[]>res.data;
    }
    return [];
  }
  async getFiles(pk_cat_prospecto: number){
    let documentos = [];
    try{
      let res: any = await lastValueFrom(this._http.get(`${this.API_URI}/documentos/${pk_cat_prospecto}`));
      let array = JSON.parse(res.data[0].documento);
      if (this.isValidResponse(res.status)) {
        for (let index = 0; index < array.length; index++) {
          let archivo = array[index];
          const obj = {
            name: archivo.name,
            base: archivo.archivo
          }
          documentos.push(obj);
        }
      }
    }catch(error){
    }
    return documentos;
  }
  async saveProspecto(prospecto: Object){
    let res: any = await lastValueFrom(this._http.post(`${this.API_URI}/prospectos`, prospecto));
    return res;
  }
  async saveDomicilio(domicilio: Object){
    await lastValueFrom(this._http.post(`${this.API_URI}/direccion`, domicilio));
  }
  async saveContacto(contacto: Object){
    await lastValueFrom(this._http.post(`${this.API_URI}/telefono`, contacto));
  }
  async saveDocumentacion(documentacion: Object){
    let res: any = await lastValueFrom(this._http.post(`${this.API_URI}/documentos`, documentacion));
  }
  async updateProspecto(fk_cat_prospecto: number, evaluacion: Object){
    let res: any = await lastValueFrom(this._http.put(`${this.API_URI}/prospectos/${fk_cat_prospecto}`, evaluacion));
  }
  private isValidResponse(status: number): boolean{
    return status === 200;
  }

}
