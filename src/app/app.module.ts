import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';



// MODULES
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMaskModule, IConfig } from 'ngx-mask'
const maskConfig: Partial<IConfig> = {
  validation: false,
};


// COMPONENTS
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ListPageComponent } from './components/list-page/list-page.component';
import { CreatePageComponent } from './components/create-page/create-page.component';
import { EvaluationPageComponent } from './components/evaluation-page/evaluation-page.component';
import { FormularioComponent } from './components/templates/formulario/formulario.component';
import { TablaComponent } from './components/templates/tabla/tabla.component';
import { HeaderComponent } from './components/templates/header/header.component';
import { SpinnerComponent } from './components/utils/spinner/spinner.component';

// SERVICES
import { ProspectoService } from './services/prospecto.service';
import { DatosService } from './services/datos.service';
import { FormService } from './services/form.service';
import { SafePipe } from './pipes/safe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ListPageComponent,
    CreatePageComponent,
    EvaluationPageComponent,
    HeaderComponent,
    FormularioComponent,
    TablaComponent,
    SafePipe,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatTableModule,
    ScrollingModule,
    MatDividerModule,
    MatRadioModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    NgxMaskModule.forRoot(maskConfig)

  ],
  providers: [
    ProspectoService,
    DatosService,
    FormService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

