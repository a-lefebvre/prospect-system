import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePageComponent } from './components/create-page/create-page.component';
import { EvaluationPageComponent } from './components/evaluation-page/evaluation-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ListPageComponent } from './components/list-page/list-page.component';

const routes: Routes = [
  {path: 'listar', component: ListPageComponent},
  {path: 'crear', component: CreatePageComponent},
  {path: 'evaluar', component: EvaluationPageComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
