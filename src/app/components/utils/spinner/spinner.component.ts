import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatosService } from 'src/app/services/datos.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SpinnerComponent>,
    @Inject(MAT_DIALOG_DATA) public waiting: Boolean,
    private _dataServ: DatosService) { }

  ngOnInit(): void {
    this._dataServ.waiting$.subscribe( waiting => {
      this.waiting = waiting;
      if (!this.waiting) {
        this.dialogRef.close();
      }
    });
  }

}
