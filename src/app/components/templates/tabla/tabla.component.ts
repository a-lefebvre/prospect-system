import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Prospecto } from 'src/app/models/prospecto.model';
import { DatosService } from 'src/app/services/datos.service';
import { ProspectoService } from 'src/app/services/prospecto.service';
import { SpinnerComponent } from '../../utils/spinner/spinner.component';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'primer_apellido', 'segundo_apellido', 'estatus'];

  prospectos: Prospecto[];
  dataSource:any;
  constructor(private _prospectoServ: ProspectoService,
    private _dataServ: DatosService,
    private _dialog: MatDialog) {
    this.prospectos = [];
  }

  ngOnInit(): void {
    this.getAllProspectos();
    this._dataServ.loadTable$.subscribe( () => {
      this.getAllProspectos();
    });
  }

  private async getAllProspectos(){
    this.prospectos = await this._prospectoServ.getProspectos();
    this.prospectos.forEach( prospecto => {
      prospecto.estatus = prospecto.estatus == 'A'? 'Activo': prospecto.estatus == 'E'? 'Enviado': 'Rechazado';
    });
    this.dataSource = this.prospectos;
  }

  loadProspecto(prospecto: any){
    this.openDialog(true);
    this._dataServ.chargeProspecto$.emit(prospecto);
  }
  openDialog(waiting: Boolean) {
    this._dialog.open(SpinnerComponent, {
      data: waiting,
      disableClose: true
    });
  }
}
