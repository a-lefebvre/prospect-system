import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Prospecto } from 'src/app/models/prospecto.model';
import { DatosService } from 'src/app/services/datos.service';
import { FormService } from 'src/app/services/form.service';
import { ProspectoService } from 'src/app/services/prospecto.service';
import { MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { SpinnerComponent } from '../../utils/spinner/spinner.component';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  @Input() vista: string = '';
  isAble: boolean = this.vista == 'crear';
  isVisible: boolean = this.vista == 'crear';
  formulario: FormGroup;
  estatus: string = '';
  documentos: any[] = [];
  files: any[] = [];
  archivos: File[] = [];
  type: string = 'application/pdf';
  constructor(private _fb: FormBuilder,
    private _dataServ: DatosService,
    private _prospectoServ: ProspectoService,
    private _router: Router,
    private _formServ: FormService,
    private _dialog: MatDialog) {
    this.formulario = this._fb.group({})
  }

  ngOnInit(): void {
    this.isAble = this.vista == 'crear';
    this.isVisible = this.vista == 'crear';
    this.initForm();
    this._dataServ.chargeProspecto$.subscribe(async (prospecto) => {
      this.initForm(prospecto);
      await this.getFiles(prospecto.pk_cat_prospecto!);
      this._dataServ.waiting$.emit(false);
    });
    this._dataServ.loadTable$.subscribe(() => {
      this.formulario.reset()
      this.files = [];
    });
  }

  private initForm(prospecto?: Prospecto) {
    let personales = {
      rfc: [{ value: prospecto?.rfc || '', disabled: !this.isAble }, [Validators.required, Validators.minLength(13)]],
      nombre: [{ value: prospecto?.nombre || '', disabled: !this.isAble }, [Validators.required, Validators.minLength(4)]],
      primer_apellido: [{ value: prospecto?.primer_apellido || '', disabled: !this.isAble }, [Validators.required, Validators.minLength(4)]],
      segundo_apellido: [{ value: prospecto?.segundo_apellido || '', disabled: !this.isAble }],
      calle: [{ value: prospecto?.calle || '', disabled: !this.isAble }, [Validators.required]],
      numero: [{ value: prospecto?.numero || '', disabled: !this.isAble }, [Validators.required]],
      colonia: [{ value: prospecto?.colonia || '', disabled: !this.isAble }, [Validators.required]],
      cp: [{ value: prospecto?.cp || '', disabled: !this.isAble }, [Validators.required]],
      telefono: [{ value: prospecto?.telefono || '', disabled: !this.isAble }, [Validators.required]],
      estatus: [{ value: prospecto?.estatus || '', disabled: !this.isAble }],
      observaciones: [{ value: prospecto?.observaciones || '', disabled: !this.isAble }]
    }
    this.formulario = this._fb.group(personales);
    this.estatus = this.formulario.get('estatus_persona')?.value || '';
  }
  private async getFiles(pk_cat_prospecto: number) {
    let res = await this._prospectoServ.getFiles(pk_cat_prospecto);
    this.files = res;

  }
  confirm(tipo_alerta: string) {
    if (tipo_alerta == 'Guardar') {
      if (this.archivos.length == 0) {
        Swal.fire({
          icon: 'info',
          confirmButtonColor: "#01ad6b",
          title: 'Notificación',
          html: 'Es Necesario al Menos un Documento del Prospecto',
        });
        return;
      }
      this.alerta('Estas seguro de guardar los datos?', tipo_alerta);
    }
    if (tipo_alerta == 'Salir') {
      let rfc = this.formulario.value.rfc
      let nombre = this.formulario.value.nombre
      let primer_apellido = this.formulario.value.primer_apellido
      let segundo_apellido = this.formulario.value.segundo_apellido
      let calle = this.formulario.value.calle
      let numero = this.formulario.value.numero
      let colonia = this.formulario.value.colonia
      let cp = this.formulario.value.cp
      let telefono = this.formulario.value.telefono
      if (this.archivos.length > 0 || rfc || nombre || primer_apellido || segundo_apellido || calle || numero || colonia || cp || telefono != '') {
        this.alerta('Si sales se perderan todos los cambios realizados, deseas continuar?', tipo_alerta);
      } else {
        this._router.navigate(['/'])
      }
    }
  }
  private async guardarInformacion() {
    this.openDialog(true);
    let prospecto = this.construirProspecto();
    let respuesta = await this._prospectoServ.saveProspecto(prospecto);
    let pk_cat_prospecto = respuesta['data'].insertId;
    let domicilio = this.construirDomicilio(pk_cat_prospecto);
    let contacto = this.construirContacto(pk_cat_prospecto);
    let documentacion = await this.construirDocumentacion(pk_cat_prospecto);
    await this._prospectoServ.saveDomicilio(domicilio);
    await this._prospectoServ.saveContacto(contacto);
    await this._prospectoServ.saveDocumentacion(documentacion);
    this._dataServ.waiting$.emit(false);
    this.formulario.reset();
    this.archivos = [];
  }

  private construirProspecto() {
    let prospecto = {
      nombre: this.formulario.value.nombre,
      primer_apellido: this.formulario.value.primer_apellido,
      segundo_apellido: this.formulario.value.segundo_apellido,
      rfc: this.formulario.value.rfc,
    }
    return prospecto;
  }
  private construirDomicilio(fk_cat_prospecto: number) {
    let domicilio = {
      calle: this.formulario.value.calle,
      numero: this.formulario.value.numero,
      colonia: this.formulario.value.colonia,
      cp: this.formulario.value.cp,
      fk_cat_prospecto: fk_cat_prospecto
    }
    return domicilio;
  }
  private construirContacto(fk_cat_prospecto: number) {
    let contacto = {
      telefono: this.formulario.value.telefono,
      fk_cat_prospecto: fk_cat_prospecto,
    }
    return contacto;
  }
  private async construirDocumentacion(fk_cat_prospecto: number) {
    let documentacion = {
      documentos: await this.convertirArchivos(this.archivos),
      fk_cat_prospecto: fk_cat_prospecto,
    }
    return documentacion;
  }
  getError(controlName: string): string {
    let error = this._formServ.getError(this.formulario, controlName);
    return error
  }
  private alerta(mensaje: string, tipo_alerta: string) {
    Swal.fire({
      title: mensaje,
      showCancelButton: true,
      confirmButtonColor: "#01ad6b",
      confirmButtonText: tipo_alerta
    }).then(async (result) => {
      if (result.isConfirmed) {
        if (tipo_alerta == 'Guardar') {
          await this.guardarInformacion();
          Swal.fire('Proceso Exitoso!', '', 'success')
        }
        if (tipo_alerta == 'Salir') {
          this._router.navigate(['/'])
        }
      }
    })
  }
  get inputDoc(): FormArray {
    return this.formulario.get('documento')?.value as FormArray;
  }
  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.files.push(file)
    }
  }
  addFile() {
    const file = this._fb.group({
      fileSource: new FormControl()
    });
    this.inputDoc.push(file);
  }
  private async convertirArchivos(archivos: File[]) {
    let obj = [];
    for (let index = 0; index < archivos.length; index++) {
      const archivo = archivos[index];
      let doc = await this.aBase64(archivo);
      let name = archivo.name;
      // let ext = this.getExtension(archivo.type);
      let docu = {
        "archivo": doc,
        "name": name
      }
      obj.push(docu);
    }
    return obj;
  }
  private aBase64 = (file: File) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  cargarArchivo(input: any) {
    for (let index = 0; index < input.files.length; index++) {
      const file = input.files[index];
      let indice = this.existeArchivo(file.name);
      if (indice == -1) {
        this.archivos.push(file);
      }
    }
  }

  borrarFile(nombre: string) {
    let index = this.existeArchivo(nombre)
    if (index > -1) {
      this.archivos.splice(index, 1);
    }
  }

  private existeArchivo(nombre: string): number {
    for (let index = 0; index < this.archivos.length; index++) {
      const element = this.archivos[index];
      if (element.name == nombre) {
        return index;
      }
    };
    return -1;
  }
  openDialog(waiting: Boolean) {
    this._dialog.open(SpinnerComponent, {
      data: waiting,
      disableClose: true
    });
  }
}
