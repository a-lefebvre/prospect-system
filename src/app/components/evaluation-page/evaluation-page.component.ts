import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Prospecto } from 'src/app/models/prospecto.model';
import { DatosService } from 'src/app/services/datos.service';
import { FormService } from 'src/app/services/form.service';
import { ProspectoService } from 'src/app/services/prospecto.service';
import Swal from 'sweetalert2';
import { SpinnerComponent } from '../utils/spinner/spinner.component';

@Component({
  selector: 'app-evaluation-page',
  templateUrl: './evaluation-page.component.html',
  styleUrls: ['./evaluation-page.component.css']
})
export class EvaluationPageComponent implements OnInit {
  evalForm: FormGroup;
  prospecto: Prospecto;
  constructor(private _fb: FormBuilder,
    private _formServ: FormService,
    private _router: Router,
    private _dataServ: DatosService,
    private _prospectoServ: ProspectoService,
    private _dialog: MatDialog) {
    this.evalForm = _fb.group({});
    this.prospecto = this.newProspecto();
  }

  ngOnInit(): void {    
    this.initForm();
    this._dataServ.chargeProspecto$.subscribe((prospecto) => {
      this.prospecto = prospecto;
    });
  }

  private initForm(){
    let evaluacion = {
      observaciones: ['', [Validators.required]],
      estatus: ['', [Validators.required]]
    }
    this.evalForm = this._fb.group(evaluacion);
  }
  confirm(tipo_alerta: string){
    if (tipo_alerta == 'Guardar') {
      if (this.prospecto.pk_cat_prospecto === undefined || this.prospecto.pk_cat_prospecto === 0 ) {
        Swal.fire({
          icon: 'info',
          confirmButtonColor: "#01ad6b",
          title: 'Notificación',
          html: 'No ha seleccionado al Prospecto',
        });
        return;
      }
      this.alerta('Estas seguro de guardar los datos?', tipo_alerta);
    }
    if (tipo_alerta == 'Salir') {
      let estatus = this.evalForm.value.estatus;
      let observaciones = this.evalForm.value.observaciones;
      if (estatus == '' && observaciones == '') {
        this._router.navigate(['/'])
      }else{
        this.alerta('Si sales se perderan todos los cambios realizados, deseas continuar?', tipo_alerta);
      }
    }
  }
  private async ActualizarInformacion(){
    this.openDialog(true);
    let evaluacion = this.evalForm.value;
    let pk_cat_prospecto = this.prospecto.pk_cat_prospecto!;
    
    await this._prospectoServ.updateProspecto(pk_cat_prospecto, evaluacion);
    this._dataServ.loadTable$.emit();
    this._dataServ.waiting$.emit(false);
    this.evalForm.reset();
    this.prospecto = this.newProspecto();
  }

  getError(controlName: string): string {
    let error = this._formServ.getError(this.evalForm, controlName);
    return error
  }
  private alerta(mensaje: string, tipo_alerta: string){
    Swal.fire({
      title: mensaje,
      showCancelButton: true,
      confirmButtonColor: "#01ad6b",
      confirmButtonText: tipo_alerta
    }).then(async (result) => {
      if (result.isConfirmed) {
        if (tipo_alerta == 'Guardar') {
          await this.ActualizarInformacion();
          Swal.fire('Proceso Exitoso!', '', 'success')
        }
        if (tipo_alerta == 'Salir') {
          this._router.navigate(['/'])
        }
      }
    })
  }

  private newProspecto(){
    return {
      nombre: '',
      primer_apellido: '',
      segundo_apellido: '',
      rfc: '',
      estatus: '',
      calle: '',
      numero: 0,
      colonia: '',
      cp: 0,
      telefono: '',
      observaciones: '',
    }
  }
  openDialog(waiting: Boolean) {
    this._dialog.open(SpinnerComponent, {
      data: waiting,
      disableClose: true
    });
  }
}
