export interface Prospecto {
    pk_cat_prospecto?: number;
    nombre: string;
    primer_apellido: string;
    segundo_apellido: string;
    rfc: string;
    estatus: string;
    calle: string;
    numero: number;
    colonia: string;
    cp: number;
    telefono: string;
    observaciones: string;
}