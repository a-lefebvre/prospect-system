export interface Domicilio{
    calle: string;
    numero: number;
    colonia: string;
    cp: number;
}